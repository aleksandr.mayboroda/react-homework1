import React from "react";
import logo from "./logo.svg";
import "./App.css";
import Popup1 from "./components/Popup_1";
import Popup2 from "./components/Popup_2";

class App extends React.Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <div>
            <Popup1 />
            <Popup2 />
          </div>
        </header>
      </div>
    );
  }
}

export default App;
