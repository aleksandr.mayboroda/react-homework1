import './Button.scss'

import React, { PureComponent } from 'react'

class Button extends PureComponent {
   

    render() {
        let {backgroundColor, text, handler} = this.props
        return (
            <button className="btn" style={{backgroundColor: backgroundColor}} onClick={() => {handler()}}>{text}</button>
        )
    }
}

export default Button