import React, { PureComponent } from "react";
import "./Modal.scss";

class Modal extends PureComponent {
  closeModalByClickOutside = (ev) => {
    if (!ev.target.closest(".modal_window")) {
      this.props.handler();
    }
  };

  closeWidow = () => {
    this.props.handler();
  };

  render() {
    const { header, text, actions, closeButton, show } = this.props;
    return (
      <>
        {show && (
          <div className="modal" onClick={this.closeModalByClickOutside}>
            <div className="modal_window">
              {closeButton && (
                <span className="modal_close" onClick={this.closeWidow}></span>
              )}
              <div className="modal_header">{header}</div>
              <div className="modal_content">{text}</div>
              {actions && <div className="modal_footer">{actions}</div>}
            </div>
          </div>
        )}
      </>
    );
  }
}

export default Modal;
