import React, { PureComponent } from "react";
import Modal from "../Modal";
import Button from "../Button";

class Popup1 extends PureComponent {
  state = {
    show: false,
  };

  modalShow = () => {
    this.setState({
      show: !this.state.show,
    });
  };

  btnOk = () => {
    alert("Button Ok Click");
  };

  btnCancel = () => {
    alert("Button Cancel Click");
    this.modalShow()
  };

  render() {
    return (
      <>
        <Button
          backgroundColor={"#e53022"}
          handler={this.modalShow}
          text={"Open first modal"}
        />
        <Modal
          header={"Do you want to delete this file?"}
          text={
            <>
              <p>
                Once you delete this file, it won\'t be possible to undo this
                action.
              </p>
              <p>Are you shure youwant to do this?</p>
            </>
          }
          actions={
            <>
              <Button
                backgroundColor={"#b3382c"}
                handler={this.btnOk}
                text={"Ok"}
              />
              <Button
                backgroundColor={"#b3382c"}
                handler={this.btnCancel}
                text={"Cancel"}
              />
            </>
          }
          closeButton
          handler={this.modalShow}
          show={this.state.show}
        />
      </>
    );
  }
}

export default Popup1;
