import React, { PureComponent } from "react";
import Modal from "../Modal";
import Button from "../Button";

class Popup2 extends PureComponent {
  state = {
    show: false,
  };

  modalShow = () => {
    this.setState({
      show: !this.state.show,
    });
  };

  btnOk = () => {
    alert("Button Ok Click");
  };

  btnCancel = () => {
    alert("Button Cancel Click");
    this.modalShow()
  };

  render() {
    return (
      <>
        <Button
          backgroundColor={"#164ad9"}
          handler={this.modalShow}
          text={"Open second modal"}
        />

        <Modal
          header={"1914 translation by H. Rackham"}
          text={
            <>
              <h3>But I must explain to you how</h3>
              <p>
                all this mistaken idea of denouncing pleasure and praising pain
                was born and I will give you a complete account of the system,
                and expound the actual teachings of the great explorer of the
                truth, the master-builder of human happiness.
              </p>
            </>
          }
          actions={
            <>
              <Button
                backgroundColor={"green"}
                handler={this.btnOk}
                text={"Ok"}
              />
              <Button
                backgroundColor={"#000"}
                handler={this.btnCancel}
                text={"Cancel"}
              />
            </>
          }
          closeButton={false}
          handler={this.modalShow}
          show={this.state.show}
        />
      </>
    );
  }
}

export default Popup2;
