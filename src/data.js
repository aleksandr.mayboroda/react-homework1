/* NOT WORKING*/
export default [
  {
    title: "Do you want to delete this file?",
    text: (
      <>
        <p>
          Once you delete this file, it won\'t be possible to undo this action.
        </p>
        <p>Are you shure youwant to do this?</p>
      </>
    ),
    footer: [
        {
            background: '#b3382c',
            text: 'Ok',
            handler: function()
            {
                console.log('Button Ok Click')
            }
        },
        {
            background: '#b3382c',
            text:'Cancel',
            handler: function()
            {
                console.log('Button Cancel Click')
            }
        }
    ]
  },
];
/* NOT WORKING*/